import Cli from './cli';
var argv = require('yargs').argv;

export default class App {
    cmdl:Object = new Cli(argv);
    constructor(readonly name:string = "TheApp") {
        this.name = name;

        this.PrintArgs();
    }

    public PrintArgs():void{
        console.log(argv);
    }

    public Run():number{
        var result = 0;

        this.PrintArgs();

        return result;
    }
}
