import argv from 'yargs';


export default class Cli {
    constructor(readonly argv: any) {
        this.argv = argv;
    }

    public GetOpt(opt:string): boolean{
        return this.argv[opt] !== undefined;
    }
}