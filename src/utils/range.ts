declare var symbol:any;
var symbol = require('symbol');

export class RangeIterator {
    value:number;
    stop:number;
  constructor(start:number, stop:number) {
    this.value = start;
    this.stop = stop;
  }

  [symbol.iterator]() { return this; }

  next() {

    var value = this.value;
    if (value < this.stop) {
      this.value++;
      return {done: false, value: value};
    } else {
      return {done: true, value: undefined};
    }
  }
}

// Return a new iterator that counts up from 'start' to 'stop'
export function range(start:number, stop:number) {
  return new RangeIterator(start, stop);
}
