/// <reference types="tape" />

import * as test from 'tape';
var App = require('app');

test('app constructor arguments', (t) => {
    t.plan(1);

    var name = "TestAppCtorArgs";
    var args = new Array<string>('-a', '-b');
    var app = new App(name);

    t.true(name === app.name, 'TestAppCtorArgs passed...');
});
