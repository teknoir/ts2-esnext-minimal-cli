declare var ava: any;

var test = require('ava');

async function fn() {
    return Promise.resolve('foo');
}

test(async (t:any) => {
    t.is(await fn(), 'foo');
});
